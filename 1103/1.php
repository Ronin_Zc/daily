<?php
for ($i = 1; $i <= 9; $i++) {
    for ($j = 1; $j <= 9; $j++) {
        if ($j > $i) {
            break;
        }
        echo $i . ' * ' . $j . ' = ' . $i * $j;
        echo '  ';
    }
    echo "\n";
}