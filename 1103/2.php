<?php
$num = 0;
fscanf(STDIN, '%d', $num);

$arr = [];

for ($i = 0; $i < $num; $i++) {
    $temp = 0;
    fscanf(STDIN, '%d', $temp);

    //判断输入数字是否存在
    if (array_key_exists($temp, $arr)) {
        $arr[$temp] += 1;
    } else {
        $arr[$temp] = 1;
    }
}

foreach ($arr as $k => $v) {
    echo $k . ' ' . $v . "\n";
}